//
//  UIFontExtension.swift
//  RestaurantsApp
//
//  Created by Adil Anwer on 5/31/18.
//  Copyright © 2018 Adil Anwer. All rights reserved.
//

import Foundation
import UIKit

extension UIFont{
    
    
    //Bold font
    static func getAppBoldFont(_ size:CGFloat = 14.0) -> UIFont{
        
        return UIFont(name: "OpenSans-ExtraBold", size: size)!
    }
    
    static func getAppRegularFont(_ size:CGFloat = 14.0) -> UIFont{
        
        return UIFont(name: "SourceSansPro-Regular", size: size)!
    }
}



