//
//  UIButtonExtension.swift
//  RestaurantsApp
//
//  Created by Adil Anwer on 5/16/18.
//  Copyright © 2018 Adil Anwer. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    /// Add image on left view
    func leftImage(image: UIImage) {
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width)
        self.titleEdgeInsets = UIEdgeInsets(top: titleEdgeInsets.top, left: 8, bottom: titleEdgeInsets.bottom, right: titleEdgeInsets.right)
        self.tintColor = UIColor.white
    }
    func rightImage(image: UIImage) {
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: image.size.width, bottom: 0, right: 0)
        self.tintColor = UIColor.white
    }
    
    func roundCornors(radius: CGFloat = 5) {
        layer.masksToBounds = true
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func setBorderColor(color: CGColor = UIColor.white.cgColor){
        
        layer.borderWidth = 2
        layer.masksToBounds = true
        layer.borderColor = color
        clipsToBounds = true
    }
    
    func setBorderColor(color: CGColor = UIColor.white.cgColor, borderWidth:CGFloat){
        
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
        layer.borderColor = color
        clipsToBounds = true
    }
    
    func round() {
        layer.masksToBounds = true
        layer.cornerRadius = self.frame.width/2
        clipsToBounds = true
    }
}
