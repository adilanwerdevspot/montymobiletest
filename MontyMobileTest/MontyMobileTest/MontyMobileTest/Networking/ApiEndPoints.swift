//
//  ApiEndPoints.swift
//  RestaurantsApp
//
//  Created by Adil Anwer on 5/18/18.
//  Copyright © 2018 Adil Anwer. All rights reserved.
//

import Foundation

enum ApiEndPoints {
    
    static let SignIn = "login"
    static let SignUp = "register"
    static let Company = "business/create"
    static let GetCompanies = "dashboard/"
    static let GetCompanyStaff = "users/"
    static let AddStaffMember = "users/create"
}
