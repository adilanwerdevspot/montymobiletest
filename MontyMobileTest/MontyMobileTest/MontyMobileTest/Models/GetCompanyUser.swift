//
//	GetCompanyUser.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GetCompanyUser : Codable {

	let active : Int?
	let clientNumber : String?
	let createdAt : String?
	let email : String?
	let id : String?
	let mobile : String?
	let name : String?
	let password : String?
	let refreshToken : String?
	let rememberMe : String?
	let speciality : String?
	let updatedAt : String?


	enum CodingKeys: String, CodingKey {
		case active = "active"
		case clientNumber = "client_number"
		case createdAt = "created_at"
		case email = "email"
		case id = "id"
		case mobile = "mobile"
		case name = "name"
		case password = "password"
		case refreshToken = "refresh_token"
		case rememberMe = "remember_me"
		case speciality = "speciality"
		case updatedAt = "updated_at"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		active = try values.decodeIfPresent(Int.self, forKey: .active) ?? Int()
		clientNumber = try values.decodeIfPresent(String.self, forKey: .clientNumber) ?? String()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? String()
		id = try values.decodeIfPresent(String.self, forKey: .id) ?? String()
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile) ?? String()
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? String()
		password = try values.decodeIfPresent(String.self, forKey: .password) ?? String()
		refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken) ?? String()
		rememberMe = try values.decodeIfPresent(String.self, forKey: .rememberMe) ?? String()
		speciality = try values.decodeIfPresent(String.self, forKey: .speciality) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	}


}
