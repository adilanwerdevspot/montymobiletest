//
//	Registration.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class Registration : Codable {

	let apiToken : String?
	let id : String?
	let mobile : String?
	let name : String?
	let refreshToken : String?
	let userEmail : String?
	let userRoles : String?


	enum CodingKeys: String, CodingKey {
		case apiToken = "api_token"
		case id = "id"
		case mobile = "mobile"
		case name = "name"
		case refreshToken = "refresh_token"
		case userEmail = "userEmail"
		case userRoles = "userRoles"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		apiToken = try values.decodeIfPresent(String.self, forKey: .apiToken) ?? String()
		id = try values.decodeIfPresent(String.self, forKey: .id) ?? String()
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile) ?? String()
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? String()
		refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken) ?? String()
		userEmail = try values.decodeIfPresent(String.self, forKey: .userEmail) ?? String()
		userRoles = try values.decodeIfPresent(String.self, forKey: .userRoles) ?? String()
	}


}