//
//	GetCompanyStaff.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GetCompanyStaff : Codable {

	let users : [GetCompanyUser]?


	enum CodingKeys: String, CodingKey {
		case users = "users"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		users = try values.decodeIfPresent([GetCompanyUser].self, forKey: .users) ?? [GetCompanyUser]()
	}


}