//
//	Businesse.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class Businesse : Codable {

	let id : String?
	let name : String?
	let shortName : String?


	enum CodingKeys: String, CodingKey {
		case id = "id"
		case name = "name"
		case shortName = "short_name"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id) ?? String()
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? String()
		shortName = try values.decodeIfPresent(String.self, forKey: .shortName) ?? String()
	}


}