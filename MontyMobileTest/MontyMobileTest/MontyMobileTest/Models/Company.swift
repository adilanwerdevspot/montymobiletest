//
//	Company.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class Company : Codable {

	let acitve : Int?
	let id : String?
	let name : String?
	let order : Int?
	let shortName : String?
	let userId : String?


	enum CodingKeys: String, CodingKey {
		case acitve = "acitve"
		case id = "id"
		case name = "name"
		case order = "order"
		case shortName = "short_name"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		acitve = try values.decodeIfPresent(Int.self, forKey: .acitve) ?? Int()
		id = try values.decodeIfPresent(String.self, forKey: .id) ?? String()
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? String()
		order = try values.decodeIfPresent(Int.self, forKey: .order) ?? Int()
		shortName = try values.decodeIfPresent(String.self, forKey: .shortName) ?? String()
		userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? String()
	}


}
