//
//	GetCompany.swift
//
//	Create by Adil Anwer on 26/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GetCompany : Codable {

	let businesses : [Businesse]?

	enum CodingKeys: String, CodingKey {
		case businesses = "businesses"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		businesses = try values.decodeIfPresent([Businesse].self, forKey: .businesses) ?? [Businesse]()
	}


}
