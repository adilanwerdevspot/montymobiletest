//
//  CompanyDetailsVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class CompanyDetailsVC: BaseVC , StoryBoardHandler{
    
    //MARK: - Storyboard name
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var nameField: UITextField?
    @IBOutlet weak var contactNumField: UITextField?
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var saveBtn: UIButton?
    
    //MARK: - Variables
    var loadedFor = ""
    var companyName = "Company Details"
    //Api and data manager for sign up
    let manager = CompanyManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if self.loadedFor == "AddCompany"{
            
            self.title = "Add new company"
        }
        else if self.loadedFor == "ViewDetails"{
            
            self.saveBtn?.isHidden = true
            self.title = companyName
            self.nameField?.text = companyName
        }
        else {
            
            self.title = companyName
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: - IBActions and other functions
extension CompanyDetailsVC{
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        //Validation
        if (!(self.emailField?.text?.isEmpty)! && !(self.nameField?.text?.isEmpty)! && !(self.contactNumField?.text?.isEmpty)!){
            
            if (self.emailField?.text?.isValidEmail())!{
                
                //Parameters for create company request
                //Name is given also as shortname because shortname field not mentioned in designs
                let requestParam = self.manager.creatCompanyParams(name: self.nameField?.text ?? "", shortName: self.nameField?.text ?? "")
                
                //Create company api call
                self.manager.api(requestParam, completion: {
                    
                    if self.manager.isSuccess {
                        
                        DispatchQueue.main.async {
                            
                            let companyVC = CompaniesVC.loadVC()
                            companyVC.baseVarCalledFrom = "Login"
                            companyVC.navigationItem.hidesBackButton = true
                            self.navigationController?.pushViewController(companyVC, animated: true)
                        }
                    }
                    else {
                        
                        //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
                    }
                })
            }
            else{
                
                Alert.showMsg(msg: "Email is not valid")
            }
        }
        else{
            
            Alert.showMsg(msg: "Please fill the required fields first.")
        }
    }
}
