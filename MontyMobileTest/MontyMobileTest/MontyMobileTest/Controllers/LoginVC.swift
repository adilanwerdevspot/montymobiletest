//
//  LoginVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class LoginVC: UIViewController , StoryBoardHandler{
    
    //MARK: - Storyboard name
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)
    //MARK: - IBOutlets
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var passwordField: UITextField?
    @IBOutlet weak var signInButton: UIButton?{
        
        didSet{
            
            signInButton?.roundCornors()
        }
    }
    
    //Api and data manager for sign up
    let manager = SignInManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Login"
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        
        //Sign in api code
        //Validation
        if (!(self.emailField?.text?.isEmpty)! && !(self.passwordField?.text?.isEmpty)!){
            
            if (self.emailField?.text?.isValidEmail())!{
                
                //Parameters for sign in request
                let requestParam = self.manager.signInParams(email: self.emailField?.text ?? "", password: self.passwordField?.text ?? "")
                
                self.manager.api(requestParam, completion: {
                    
                    if self.manager.isSuccess {
                        
                        DispatchQueue.main.async {
                            
                            let companyVC = CompaniesVC.loadVC()
//                            companyDetailsVC.loadedFor = ""
                            companyVC.navigationItem.setHidesBackButton(true, animated:false);
                            companyVC.baseVarCalledFrom = "Login"
                            self.navigationController?.pushViewController(companyVC, animated: true)
                        }
                    }
                    else {
                        
                        //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
                    }
                })
                
            }
            else{
                
                Alert.showMsg(msg: "Email is not valid")
            }
        }
        else {
            
            Alert.showMsg(msg: "Please fill the required fields first.")
        }
    }
    
}
