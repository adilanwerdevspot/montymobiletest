//
//  RegisterVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController , StoryBoardHandler{
    
    /* We are using AppStoryboardAndNavigation utility for getting and loading the views from their storyboards. For it we have confirmed this class to StoryboardHandler protocol and then setted its storyboard name. By the help of it we do not need to write every time viewcontrollers storybaord id and storyboard name. Also it is very use full when we have multiple storyboards and multiple developers are working on project. Now we just need to call loadVC method on this viewcontroller to get its instance*/
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)

    //MARK: - IBOutlets
    @IBOutlet weak var signUpButton: UIButton?{
        
        didSet{
            
            signUpButton?.roundCornors()
        }
    }
    
    @IBOutlet weak var firstnameField: UITextField?
    @IBOutlet weak var lastnameField: UITextField?
    @IBOutlet weak var usernameField: UITextField?
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var passwordField: UITextField?
    @IBOutlet weak var cnfrmPswrdField: UITextField?
    @IBOutlet weak var checkBoxBtn: UIButton?
    
    //MARK: - Variables
    //Api and data manager for sign up
    let manager = SignUpManager()
    
    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Registration"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

//MARK: - IBActions and other fucntions
extension RegisterVC{
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        
        
        //Sign up api code
        //Validation
        if (!(self.emailField?.text?.isEmpty)! && !(self.passwordField?.text?.isEmpty)! && !(self.cnfrmPswrdField?.text?.isEmpty)! && !(self.firstnameField?.text?.isEmpty)! && !(self.lastnameField?.text?.isEmpty)! && !(self.usernameField?.text?.isEmpty)!) && self.checkBoxBtn?.currentImage == UIImage(named:"checked"){
            
            if (self.emailField?.text?.isValidEmail())!{
                
                if ((self.passwordField?.text!.count)! > 5) {
                
                if(self.passwordField?.text?.elementsEqual((self.cnfrmPswrdField?.text)!) == true){
                    
                    //Parameters for sign up request
                    //Phone num hardcoded because its field not mentioned in designs
                    let requestParam = self.manager.signUpParams(email: self.emailField?.text ?? "", password: self.passwordField?.text ?? "", name: self.firstnameField?.text ?? "" + (self.lastnameField?.text!)!, mobile: "123456789")
                    
                    //Sign up api call
                    self.manager.api(requestParam, completion: {
                        
                        if self.manager.isSuccess {
                            
                            DispatchQueue.main.async {
                                
                                let companyDetailsVC = CompanyDetailsVC.loadVC()
                                companyDetailsVC.loadedFor = "AddCompany"
                                companyDetailsVC.baseVarCalledFrom = "Login"
                                companyDetailsVC.navigationItem.setHidesBackButton(true, animated:false);
                                self.navigationController?.pushViewController(companyDetailsVC, animated: true)
                            }
                        }
                        else {
                            
                            //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
                        }
                    })
                }
                else{
                    
                    Alert.showMsg(msg: "Password does not match")
                }
                }
                else{
                    Alert.showMsg(msg: "Password should be six characters long")
                }
            }
            else{
                
                Alert.showMsg(msg: "Email is not valid")
            }
        }
        else{
            
            Alert.showMsg(msg: "Please fill the required fields first. And agree with terms")
        }
    }
    
    @IBAction func checkBoxButtonPressed(_ sender: UIButton) {
        
        if sender.currentImage == UIImage(named:"uncheckBox"){
            
         //checked
            
            self.checkBoxBtn?.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
        }
        else {
            
            self.checkBoxBtn?.setImage(UIImage(named:"uncheckBox"), for: UIControl.State.normal)
        }
    }
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        
        let loginVC = LoginVC.loadVC()
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
}
