//
//  StaffMemberDetailsVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class StaffMemberDetailsVC: BaseVC , StoryBoardHandler{
    
    //MARK: - Storyboard name
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)

    //MARK: - IBOutlets
    
    @IBOutlet weak var nameField: UITextField?
    @IBOutlet weak var contactNumField: UITextField?
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var positionField: UITextField?
    @IBOutlet weak var saveBtn: UIButton?
    
    //MARK: - Variables
    var loadedFor = ""
    var staffMemberInfo:GetCompanyUser? = nil
    var companyName = ""
    var companyId = ""
    
    //Api and data manager for sign up
    let manager = AddNewStaffMember()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if self.loadedFor == "AddStaff"{
            
            self.title = "Add staff member"
        }
        else if self.loadedFor == "ViewDetails"{
            
            self.saveBtn?.isHidden = true
            self.title = self.staffMemberInfo?.name ?? "Staff Member"
            self.nameField?.text = self.staffMemberInfo?.name ?? "Staff Member"
            self.contactNumField?.text = self.staffMemberInfo?.mobile ?? ""
            self.emailField?.text = self.staffMemberInfo?.email ?? ""
            self.positionField?.text = self.staffMemberInfo?.speciality ?? ""
        }
        else {
            
            self.title = self.staffMemberInfo?.name ?? "Staff Member"
        }
    }
    

    @IBAction func saveBtnPressed(_ sender: Any) {
        
        //Save staff member api code
        //Validation
        if (!(self.emailField?.text?.isEmpty)! && !(self.nameField?.text?.isEmpty)! && !(self.contactNumField?.text?.isEmpty)!){
            
            if (self.emailField?.text?.isValidEmail())!{
                
                let requestParam = self.manager.addNewStaffParams(email: self.emailField?.text ?? "", name: self.nameField?.text ?? "", mobile: self.contactNumField?.text ?? "", businessId: self.companyId)
                
                self.manager.api(requestParam, completion: {
                    
                    if self.manager.isSuccess {
                        
                        DispatchQueue.main.async {
                            
                            Alert.showMsg(msg: "Staff member added succesfully.")
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else {
                        
                        //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
                    }
                })
            }
            else{
                
                Alert.showMsg(msg: "Email is not valid")
            }
        }
        else{
            
            Alert.showMsg(msg: "Please fill the required fields first.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
