//
//  StaffMemberCell.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/26/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class StaffMemberCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var editBtn: UIButton?
    @IBOutlet weak var deleteBtn: UIButton?
    @IBOutlet weak var viewDetailsBtn: UIButton?
    @IBOutlet weak var companyNameLbl: UILabel?
    @IBOutlet weak var position: UILabel?
    
    //MARK: - Variables
    var parentVC:StaffMembersVC? = nil
    var staffMemberInfo:GetCompanyUser? = nil
    
    func config(staffMemberInfo:GetCompanyUser!){
        
        self.staffMemberInfo = staffMemberInfo
        self.companyNameLbl?.text = self.staffMemberInfo?.name ?? ""
    }
}

//MARK: - IBActions and other methods
extension StaffMemberCell{
    
    @IBAction func editBtnPressed(_ sender: Any) {
        
        //        let companyVC = CompanyDetailsVC.loadVC()
        //        self.parentVC?.navigationController?.pushViewController(companyVC, animated: true)
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        
        
    }
    
    @IBAction func viewDetailsBtnPressed(_ sender: Any) {
        
        let companyVC = StaffMemberDetailsVC.loadVC()
        companyVC.loadedFor = "ViewDetails"
        companyVC.staffMemberInfo =  self.staffMemberInfo
        self.parentVC?.navigationController?.pushViewController(companyVC, animated: true)
    }
}
