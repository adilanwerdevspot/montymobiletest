//
//  StaffMembersVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class StaffMembersVC: BaseVC , StoryBoardHandler{
    
    //MARK: - Storyboard name
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)

    //MARK: - IBOutlets
    @IBOutlet weak var staffCollectionView: UICollectionView?{
        
        didSet{
            
            staffCollectionView?.delegate = self
            staffCollectionView?.dataSource = self
        }
    }
    //MARK: - Variables
    var companyId:String? = nil
    let staffCellId = "StaffMemberCell"
    
    //Api and data manager for sign up
    let manager = GetCompanyStaffManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = self.staffCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            
            flowLayout.itemSize = CGSize(width: self.view.frame.size.width * 0.736,height: self.view.frame.size.height * 0.435)
        }
        
        // Do any additional setup after loading the view.
        self.title = "Staff Members"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.getCompanyStaff()
    }
}

//MARK: - IBActions and other functions
extension StaffMembersVC{
    
    //MARK: - Api call to get company staff
    func getCompanyStaff() {
        
        //Parameters for company staff request
        let requestParam = self.manager.getCompaniesStaffParams(self.companyId ?? "")
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                
                DispatchQueue.main.async {
                    
                    if self.manager.companyResponce?.users?.count ?? 0 == 1{
                        
                        if let flowLayout = self.staffCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
                            
                            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: UIScreen.main.bounds.size.width * 0.0966, bottom: 0, right: 0)
                        }
                    }
                    else if self.manager.companyResponce?.users?.count ?? 0 > 1{
                        
                        if let flowLayout = self.staffCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
                            
                            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
                        }
                    }
                    
                    self.staffCollectionView?.reloadData()
                }
            }
            else {
                
                //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
            }
        })
    }
    
    @IBAction func addNewStaffBtnPressed(_ sender: Any) {
        
        let staffMemberDetailsVC = StaffMemberDetailsVC.loadVC()
        staffMemberDetailsVC.loadedFor = "AddStaff"
        staffMemberDetailsVC.companyId = self.companyId ?? ""
        self.navigationController?.pushViewController(staffMemberDetailsVC, animated: true)
    }
}

//MARK: - UICollectionView delegate and datasource
extension StaffMembersVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.manager.companyResponce?.users?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.staffCollectionView?.dequeueReusableCell(withReuseIdentifier: self.staffCellId, for: indexPath) as! StaffMemberCell
        
        cell.parentVC = self
        cell.config(staffMemberInfo: self.manager.companyResponce?.users?[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let companyVC = StaffMemberDetailsVC.loadVC()
        companyVC.loadedFor = "ViewDetails"
        companyVC.staffMemberInfo = self.manager.companyResponce?.users?[indexPath.item]
        self.navigationController?.pushViewController(companyVC, animated: true)
    }
    
}
