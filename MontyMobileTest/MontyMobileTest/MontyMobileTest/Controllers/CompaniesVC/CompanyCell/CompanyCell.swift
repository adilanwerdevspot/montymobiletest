//
//  CompanyCell.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/26/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class CompanyCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var editBtn: UIButton?
    @IBOutlet weak var deleteBtn: UIButton?
    @IBOutlet weak var viewDetailsBtn: UIButton?
    @IBOutlet weak var companyNameLbl: UILabel?
    //MARK: - Variables
    var parentVC:CompaniesVC? = nil
    
    func config(companyName:String!){
        
        self.companyNameLbl?.text = companyName
    }
}

//MARK: - IBActions and other methods
extension CompanyCell{
    
    @IBAction func editBtnPressed(_ sender: Any) {
        
//        let companyVC = CompanyDetailsVC.loadVC()
//        self.parentVC?.navigationController?.pushViewController(companyVC, animated: true)
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        
        
    }
    
    @IBAction func viewDetailsBtnPressed(_ sender: Any) {
        
        let companyVC = CompanyDetailsVC.loadVC()
        companyVC.loadedFor = "ViewDetails"
        companyVC.companyName = self.companyNameLbl?.text ?? "Company Details"
        self.parentVC?.navigationController?.pushViewController(companyVC, animated: true)
    }
}
