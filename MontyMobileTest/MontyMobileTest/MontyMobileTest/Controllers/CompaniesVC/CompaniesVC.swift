//
//  CompaniesVC.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/25/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import UIKit

class CompaniesVC: BaseVC , StoryBoardHandler{

    //MARK: - Storyboard name
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.MainStoryboad.rawValue, nil)
    
    //MARK: - IBOutlets
    @IBOutlet weak var companiesCollectionView: UICollectionView?{
        
        didSet{
            
            companiesCollectionView?.delegate = self
            companiesCollectionView?.dataSource = self
        }
    }
    @IBOutlet weak var companiesLbl: UILabel?
    //MARK: - Variables
    let companyCellId = "CompanyCell"
    //Api and data manager for sign up
    let manager = GetCompaniesManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Dashboard"
        
        if let flowLayout = self.companiesCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            
            flowLayout.itemSize = CGSize(width: self.view.frame.size.width * 0.736,height: self.view.frame.size.height * 0.435)
        }
        
        //Parameters for get companies request
        let requestParam = self.manager.getCompaniesParams()
        //Make company api call
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                
                DispatchQueue.main.async {
                    
                    if self.manager.companyResponce?.businesses?.count ?? 0 == 1{
                        
                        if let flowLayout = self.companiesCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
                            
                            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: UIScreen.main.bounds.size.width * 0.0966, bottom: 0, right: 0)
                        }
                    }
                    else if self.manager.companyResponce?.businesses?.count ?? 0 > 1{
                        
                        if let flowLayout = self.companiesCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
                            
                            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
                        }
                    }
                    
                    self.companiesLbl?.text  = "Total " + String(self.manager.companyResponce?.businesses?.count ?? 0) +  " Companies"
                    
                    self.companiesCollectionView?.reloadData()
                }
            }
            else {
                
                //                GlobalFunctions.setupNoRecordsBackgroundLabelForCollectionView(self.booksCollectionView!)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

//MARK: - IBActions and other functions
extension CompaniesVC : UITextFieldDelegate{
    
    @IBAction func addNewCompanyBtnPressed(_ sender: Any) {
        
        let companyVC = CompanyDetailsVC.loadVC()
        companyVC.loadedFor = "AddCompany"
        self.navigationController?.pushViewController(companyVC, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

//MARK: - UICollectionView delegate and datasource
extension CompaniesVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.manager.companyResponce?.businesses?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.companiesCollectionView?.dequeueReusableCell(withReuseIdentifier: self.companyCellId, for: indexPath) as! CompanyCell
        
        cell.parentVC = self
        cell.config(companyName: self.manager.companyResponce?.businesses?[indexPath.item].name ?? "")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let staffVC = StaffMembersVC.loadVC()
        staffVC.companyId = self.manager.companyResponce?.businesses?[indexPath.item].id ?? ""
        self.navigationController?.pushViewController(staffVC, animated: true)
    }
    
}
