//
//  AddNewStaffMember.swift
//  MontyMobileTest
//
//  Created by Adil Anwer on 5/26/19.
//  Copyright © 2019 Adil Anwer. All rights reserved.
//

import Foundation
import Alamofire

class AddNewStaffMember:AFManagerProtocol {
    
    var isSuccess   = false
    var signUpResponce     : Registration?
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        self.isSuccess = false
        
        //Request
        AFWrapper.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            if let responceDictnry = response as? Dictionary<String,Any> {
                //Come in this block when some logical error occur in api
                self.isSuccess = false
                if (responceDictnry["error"] != nil){
                    
                    DispatchQueue.main.async {
                        
                       
                        let message = responceDictnry["error_description"] ?? "Error in api"
                        Alert.showMsg(msg: message as! String)
                        
                        
                    }
                }
                else if let errorMsg = responceDictnry["message"]{
                    
                    Alert.showMsg(msg: errorMsg as! String)
                }
            }
            else {
                
                guard let data = response as? Data else { return }
                
                do {
                    
                    let decoder = JSONDecoder()
//                    let model = try decoder.decode(Registration.self, from: data)
//
//                    if let token = model.apiToken{
//
//                        let accessToken = token
//                        UserDefaults.standard.set(accessToken, forKey: "userAccessToken")
//                        UserDefaults.standard.synchronize()
//                        UserDefaults.standard.set(model.id, forKey: "userId")
//                        UserDefaults.standard.synchronize()
//                        AFWrapper.shared.setAccessTokenOnAuthorization(accessToken: accessToken)
//                    }
                    
                    self.isSuccess = true
//                    self.signUpResponce = model
                    
                } catch let err {
                    print("Err", err)
                }
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

//Mark : - Extension of sign up manager to create parameters
extension AddNewStaffMember {
    
    //Function to create parameters
    func addNewStaffParams(email: String, name:String, mobile:String, businessId:String) -> AFParam {
        
        let headers: [String : String]? = [:]
        let parameters = [
            "email": email,
            "password": "123456",
            "name": name,
            "mobile": mobile,
            "business_id":businessId,
            "active":"1"
            ] as [String : AnyObject]
        
        let param = AFParam(endpoint: ApiEndPoints.AddStaffMember, params: parameters, headers: headers!, method: .post, parameterEncoding: URLEncoding(destination: .methodDependent), images: [])
        return param
    }
}
