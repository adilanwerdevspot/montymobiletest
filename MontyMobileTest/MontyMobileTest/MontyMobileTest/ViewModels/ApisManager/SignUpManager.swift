//
//  SignUpManager.swift
//  RestaurantsApp
//
//  Created by Adil Anwer on 5/24/18.
//  Copyright © 2018 Adil Anwer. All rights reserved.
//

import Foundation
import Alamofire

class SignUpManager:AFManagerProtocol {
    
    var isSuccess   = false
    var signUpResponce     : Registration?
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        self.isSuccess = false
        
        //Request
        AFWrapper.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            if let responceDictnry = response as? Dictionary<String,Any> {
                //Come in this block when some logical error occur in api
                self.isSuccess = false
                if (responceDictnry["error"] != nil){
                    
                    DispatchQueue.main.async {
                        

                        let message = responceDictnry["error_description"] ?? "Error in api"
                        Alert.showMsg(msg: message as! String)
                        

                    }
                }
                else if let errorMsg = responceDictnry["message"]{
                    
                    Alert.showMsg(msg: errorMsg as! String)
                }
            }
            else {
                
                guard let data = response as? Data else { return }
                
                do {
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Registration.self, from: data)
                    
                    if let token = model.apiToken{
                        
                        let accessToken = token
                        UserDefaults.standard.set(accessToken, forKey: "userAccessToken")
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(model.id, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        AFWrapper.shared.setAccessTokenOnAuthorization(accessToken: accessToken)
                    }
                    
                    self.isSuccess = true
                    self.signUpResponce = model
                    
                } catch let err {
                    print("Err", err)
                }
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

//Mark : - Extension of sign up manager to create parameters
extension SignUpManager {
    
    //Function to create parameters
    func signUpParams(email: String, password:String, name:String, mobile:String) -> AFParam {
        
        let headers: [String : String]? = [:]
        let parameters = [
            "email": email,
            "password": password,
            "name": name,
            "mobile": mobile
            ] as [String : AnyObject]
        
        let param = AFParam(endpoint: ApiEndPoints.SignUp, params: parameters, headers: headers!, method: .post, parameterEncoding: URLEncoding(destination: .methodDependent), images: [])
        return param
    }
}

extension SignUpManager {
    
    //    func numberOfItemsToDisplay() -> Int {
    //
    //        return arrData?.count ?? 0
    //    }
    //
    //    func displayTitle(for indexPath: IndexPath) -> String {
    //
    //        return arrData?[indexPath.row].title ?? ""
    //    }
    
}
