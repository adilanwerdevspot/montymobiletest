//
//  SignInManager.swift
//  RestaurantsApp
//
//  Created by Adil Anwer on 5/18/18.
//  Copyright © 2018 Adil Anwer. All rights reserved.
//

import Foundation
import Alamofire

class SignInManager:AFManagerProtocol {
    
    var isSuccess   = false
    var loginResponce     : Login?
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        self.isSuccess = false
        
        //Request
        AFWrapper.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            if let responceDictnry = response as? Dictionary<String,Any> {
                //Come in this block when some logical error occur in api
                self.isSuccess = false
                if (responceDictnry["error"] != nil){
                    
                    DispatchQueue.main.async {
                        
                        
                        let message = responceDictnry["error_description"] ?? "Error in api"
                        Alert.showMsg(msg: message as! String)
                        
                        
                    }
                }
                    /*{
                     "message" : "These credentials do not match our records."
                     }*/
                else if let errorMsg = responceDictnry["message"]{
                    
                    Alert.showMsg(msg: errorMsg as! String)
                }
            }
            else {
                
                guard let data = response as? Data else { return }
                
                do {
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Login.self, from: data)
                    if let token = model.apiToken{
                        
                        let accessToken = token
                        UserDefaults.standard.set(accessToken, forKey: "userAccessToken")
                        UserDefaults.standard.synchronize()
                        AFWrapper.shared.setAccessTokenOnAuthorization(accessToken: accessToken)
                    }
                    self.isSuccess = true
                    self.loginResponce = model
                } catch let err {
                    print("Err", err)
                }
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

//Mark : - Extension of sign up manager to create parameters
extension SignInManager {
    
    //Function to create parameters
    func signInParams(email: String, password:String) -> AFParam {
        
        let headers: [String : String]? = [:]
        let parameters = [
            "email": email,
            "password": password] as [String : AnyObject]
        
        let param = AFParam(endpoint: ApiEndPoints.SignIn, params: parameters, headers: headers!, method: .post, parameterEncoding: URLEncoding(destination: .methodDependent), images: [])
        return param
    }
}
